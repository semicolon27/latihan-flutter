import 'package:flutter/material.dart';
import 'package:latihan_flutter/screens/LoginScreen.dart';
import 'package:latihan_flutter/screens/ProfileScreen.dart';

void main() {
  runApp(new MaterialApp(
    title : "Latihan Pertama",
    initialRoute: '/',
    routes: {
      '/': (context) => LoginScreen(),
      '/profile': (context) => ProfileScreen()
    },
  ));
}

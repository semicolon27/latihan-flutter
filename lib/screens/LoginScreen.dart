import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: new Center(
        child: new LoginForm(),
      )
    );
  }
}

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  TextStyle style = TextStyle(fontFamily: "Poppins", fontSize: 20.0);

  @override
  void dispose(){
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Form(
        key: _formKey,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: SizedBox(
                  height: 155.0,
                  child: Image.network(" https://picsum.photos/200/300")
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom:10.0),
                child: new TextFormField(
                  controller: usernameController,
                  obscureText: false,
                  style: style,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Username",
                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                  ),
                  validator: (value) {
                    if(value.isEmpty){
                      return "Jangan Kosong";
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom:10.0),
                child: new TextFormField(
                  controller: passwordController,
                  obscureText: false,
                  style: style,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                    hintText: "Password",
                    border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                  ),
                  validator: (value) {
                    if(value.isEmpty){
                      return "Jangan Kosong";
                    }
                    return null;
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 10.0),
                child: Material(
                  elevation: 5.0,
                  borderRadius: BorderRadius.circular(30.0),
                  color: Color(0xff01A0C7),
                    child: MaterialButton(
                      minWidth: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      onPressed: () {
                        if(usernameController.text == "admin" && passwordController.text == "123"){
                          Navigator.pushNamed(context, '/profile');
                          // Scaffold.of(context).showSnackBar(SnackBar(content: Text("Loading")));
                        }
                        else if(_formKey.currentState.validate()){
                          Scaffold.of(context).showSnackBar(SnackBar(content: Text("Username atau Password Salah")));
                        }
                      },
                      child: Text("Login",
                        textAlign: TextAlign.center,
                        style: style.copyWith(
                          color: Colors.white, fontWeight: FontWeight.bold,
                        ),
                      ),
                  ),
                ),
              ),
            ],
          ),
        )
      ),
    );
  }
}


// To parse this JSON data, do
//
//     final post = postFromJson(jsonString);
import 'package:http/http.dart' as http;
import 'dart:convert';

Future<Post> fetchPost() async {
  final res = await http.get("http://192.168.8.106:7007/api/");
  if(res.statusCode != 500) {
    return Post.fromJson(json.decode(res.body));
  }else{
    throw Exception("Failed to load the data");
  }
}

Post postFromJson(String str) => Post.fromJson(json.decode(str));

String postToJson(Post data) => json.encode(data.toJson());

class Post {
    String status;
    String messages;
    List<Datum> data;

    Post({
        this.status,
        this.messages,
        this.data,
    });

    factory Post.fromJson(Map<String, dynamic> json) => Post(
        status: json["status"],
        messages: json["messages"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "messages": messages,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
    };
}

class Datum {
    String nis;
    String nama;
    int jk;
    int idKelas;
    Kela kela;

    Datum({
        this.nis,
        this.nama,
        this.jk,
        this.idKelas,
        this.kela,
    });

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        nis: json["nis"],
        nama: json["nama"],
        jk: json["jk"],
        idKelas: json["id_kelas"],
        kela: Kela.fromJson(json["kela"]),
    );

    Map<String, dynamic> toJson() => {
        "nis": nis,
        "nama": nama,
        "jk": jk,
        "id_kelas": idKelas,
        "kela": kela.toJson(),
    };
}

class Kela {
    int idKelas;
    int idProdi;
    String namaKelas;
    String walikelas;
    Prodi prodi;

    Kela({
        this.idKelas,
        this.idProdi,
        this.namaKelas,
        this.walikelas,
        this.prodi,
    });

    factory Kela.fromJson(Map<String, dynamic> json) => Kela(
        idKelas: json["id_kelas"],
        idProdi: json["id_prodi"],
        namaKelas: json["nama_kelas"],
        walikelas: json["walikelas"],
        prodi: Prodi.fromJson(json["prodi"]),
    );

    Map<String, dynamic> toJson() => {
        "id_kelas": idKelas,
        "id_prodi": idProdi,
        "nama_kelas": namaKelas,
        "walikelas": walikelas,
        "prodi": prodi.toJson(),
    };
}

class Prodi {
    int idProdi;
    String namaProdi;

    Prodi({
        this.idProdi,
        this.namaProdi,
    });

    factory Prodi.fromJson(Map<String, dynamic> json) => Prodi(
        idProdi: json["id_prodi"],
        namaProdi: json["nama_prodi"],
    );

    Map<String, dynamic> toJson() => {
        "id_prodi": idProdi,
        "nama_prodi": namaProdi,
    };
}

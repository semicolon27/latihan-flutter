import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:latihan_flutter/screens/Model.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(title: Text("Profil"),),
      body: DataList(),
    );
  }
}

class DataList extends StatefulWidget {
  DataList({Key key}) : super(key: key);

  @override
  _DataListState createState() => _DataListState();
}

class _DataListState extends State<DataList> {
  Future<Post> post;

  @override
  void initState(){
    super.initState();
    post = fetchPost();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
       child: FutureBuilder<Post>(
        future: post,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return ListView(children: snapshot.data.data.map((v){
              return new Card(
              child: new Column(
                children: <Widget>[
                  Text(v.nis),
                  Text(v.nama),
                  Text(v.kela.namaKelas),
                  Text(v.jk == 0 ? "Perempuan" : "Laki - laki"),
                ],
              ));
            }).toList());
          } else if (snapshot.hasError) {
            return Text("error : ${snapshot.error}");
          }

          // By default, show a loading spinner.
          return CircularProgressIndicator();
        },
      ),
    );
  }
}
